import Vue from 'vue'
import Router from 'vue-router'
const Login = resolve => require(['/page/Login/login'], resolve)
const Register = resolve => require(['/page/Login/Register'], resolve)
const Retrieve = resolve => require(['/page/Login/Retrieve'], resolve)
const OrderForm = resolve => require(['/page/OrderForm/OrderForm'], resolve)
const GoodsList = resolve => require(['/page/Goods/GoodsList'], resolve)
const ProductDetails = resolve => require(['/page/Goods/ProductDetails'], resolve)
const Index = resolve => require(['/page/Index/Index'], resolve)
const User = resolve => require(['/page/UserCenter/User'], resolve)
const OrderForms = resolve => require(['/page/OrderForm/OrderForms'], resolve)
const OrderFormss = resolve => require(['/page/OrderForm/OrderFormss'], resolve)
const Uploading = resolve => require(['/page/Uploading/Uploading'], resolve)
const Release = resolve => require(['/page/Uploading/Release'], resolve)
const UserIndex = resolve => require(['/page/UserCenter/UserIndex'], resolve)
const TransactionManagement = resolve => require(['/page/TheSellerCenter/TransactionManagement'], resolve)
const RelReturnGoods = resolve => require(['/page/ReturnGoods/ReturnGoods'], resolve)
const RelReturn = resolve => require(['/page/ReturnGoods/RelReturn'], resolve)
const Returns = resolve => require(['/page/ReturnGoods/Returns'], resolve)
const Relreturns = resolve => require(['/page/ReturnGoods/Relreturns'], resolve)
const Money = resolve => require(['/page/ReturnGoods/Money'], resolve)
const Moneys = resolve => require(['/page/ReturnGoods/Moneys'], resolve)
const Choice = resolve => require(['/page/ReturnGoods/Choice'], resolve)
const Search = resolve => require(['/page/Search/Search'], resolve)
const Account = resolve => require(['/page/Account/Account'], resolve)
const Accounts = resolve => require(['/page/Account/Accounts'], resolve)
const Accountr = resolve => require(['/page/Account/Accountr'], resolve)


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Release',
      component: Release
    },
    {path: '/Index', name: 'Index', component: Index},
    {path: '/Login', name: 'Login', component: Login},
    {path: '/Register', name: 'Register', component: Register},
    {path: '/Retrieve', name: 'Retrieve', component: Retrieve},
    {path: '/OrderForm', name: 'OrderForm', component: OrderForm},
    {path: '/GoodsList', name: 'GoodsList', component: GoodsList},
    {path: '/ProductDetails', name: 'ProductDetails', component: ProductDetails},
    {path: '/User', name: 'User', component: User},
    {path: '/UserIndex', name: 'UserIndex', component: UserIndex},
    {path: '/OrderForm', name: 'OrderForms', component: OrderForms},
    {path: '/OrderForm', name: 'OrderForms', component: OrderFormss},
    {path: '/Uploading', name: 'Uploading', component: Uploading},
    {path: '/Release', name: 'Release', component: Release},
    {path: '/TransactionManagement', name: 'TransactionManagement', component: TransactionManagement}，
    {path: '/RelReturnGoods', name: 'RelReturnGoods', component: RelReturnGoods},
    {path: '/RelReturnGoods', name: 'RelReturnGoods', component: RelReturn},
    {path: '/RelReturnGoods', name: 'RelReturnGoods', component: Returns},
    {path: '/RelReturnGoods', name: 'RelReturnGoods', component: Relreturns},
    {path: '/RelReturnGoods', name: 'RelReturnGoods', component: Money},
    {path: '/RelReturnGoods', name: 'RelReturnGoods', component: Moneys},
    {path: '/RelReturnGoods', name: 'RelReturnGoods', component: Choice},
    {path: '/Search', name: 'Search', component: Search},
    {path: '/Account', name: 'Account', component: Account},
    {path: '/Account', name: 'Account', component: Accounts},
    {path: '/Account', name: 'Account', component: Accountr}
  ]
})
